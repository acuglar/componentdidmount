import { Component } from "react";

class Character extends Component {
  render() {
    const { name, image, id } = this.props
    return (
      <div key={id}>
        <div style={{ border: "1px solid yellow" }}>
          <label>{name}</label>
          <img src={image} alt={name} />
        </div>
      </div>
    )
  }
}

export default Character;