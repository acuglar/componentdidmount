import { Component } from 'react';
import Character from "../Character";

class CharacterList extends Component {
  render() {
    const { list } = this.props
    return (
      <div>
        <h3>Meus personagens</h3>
        <div>
          {list.map((char) => <Character id={char.id} name={char.name} image={char.image}></Character>)}
        </div>
      </div>
    )
  }
}

export default CharacterList;