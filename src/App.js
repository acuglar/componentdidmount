import { Component } from 'react';
import './App.css';
import CharacterList from './components/CharacterList';

class App extends Component {
  state = {
    characterList: [],
  }

  componentDidMount() {
    fetch("https://rickandmortyapi.com/api/character")  //requisição fetch: função tipo promisse
      //.then(response => console.log(response));  response parâmetro 
      .then((response) => response.json())  //se response, then normaliza response para json  
      //.then(response => console.log(response.results));
      .then((response) => {
        this.setState({ characterList: response.results });  //results propriedade de response no caso, verificável no json da api
      })
      .catch(error => console.log(error))
  }
  render() {
    console.log(this.state.characterList)
    return (
      <div className="App">
        <header className="App-header">
          <CharacterList list={this.state.characterList}></CharacterList>
        </header>
      </div>
    )
  }
}

export default App;

/*
1. state recebendo lista
2. requisição fetch e setState lista

*/
